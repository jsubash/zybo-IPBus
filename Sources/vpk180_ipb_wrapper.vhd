--Copyright 1986-2022 Xilinx, Inc. All Rights Reserved.
--Copyright 2022-2023 Advanced Micro Devices, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2023.2.1 (lin64) Build 4081461 Thu Dec 14 12:22:04 MST 2023
--Date        : Tue Feb 20 14:18:16 2024
--Host        : daq11.hep.ucl.ac.uk running 64-bit unknown
--Command     : generate_target vpk180_ipb_wrapper.bd
--Design      : vpk180_ipb_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity vpk180_ipb_wrapper is
  port (
    BRAM_PORTA_addr : out STD_LOGIC_VECTOR ( 12 downto 0 );
    BRAM_PORTA_clk : out STD_LOGIC;
    BRAM_PORTA_din : out STD_LOGIC_VECTOR ( 63 downto 0 );
    BRAM_PORTA_dout : in STD_LOGIC_VECTOR ( 63 downto 0 );
    BRAM_PORTA_en : out STD_LOGIC;
    BRAM_PORTA_rst : out STD_LOGIC;
    BRAM_PORTA_we : out STD_LOGIC_VECTOR ( 7 downto 0 );
    BRAM_PORTB_addr : out STD_LOGIC_VECTOR ( 12 downto 0 );
    BRAM_PORTB_clk : out STD_LOGIC;
    BRAM_PORTB_din : out STD_LOGIC_VECTOR ( 63 downto 0 );
    BRAM_PORTB_dout : in STD_LOGIC_VECTOR ( 63 downto 0 );
    BRAM_PORTB_en : out STD_LOGIC;
    BRAM_PORTB_rst : out STD_LOGIC;
    BRAM_PORTB_we : out STD_LOGIC_VECTOR ( 7 downto 0 );
    axi_clk_o : out STD_LOGIC;
    bd_arst_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    ch0_lpddr4_trip1_ca_a : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch0_lpddr4_trip1_ca_b : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch0_lpddr4_trip1_ck_c_a : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_c_b : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_t_a : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_t_b : out STD_LOGIC;
    ch0_lpddr4_trip1_cke_a : out STD_LOGIC;
    ch0_lpddr4_trip1_cke_b : out STD_LOGIC;
    ch0_lpddr4_trip1_cs_a : out STD_LOGIC;
    ch0_lpddr4_trip1_cs_b : out STD_LOGIC;
    ch0_lpddr4_trip1_dmi_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dmi_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dq_a : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch0_lpddr4_trip1_dq_b : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch0_lpddr4_trip1_dqs_c_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dqs_c_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dqs_t_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dqs_t_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_reset_n : out STD_LOGIC;
    ch1_lpddr4_trip1_ca_a : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch1_lpddr4_trip1_ca_b : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch1_lpddr4_trip1_ck_c_a : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_c_b : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_t_a : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_t_b : out STD_LOGIC;
    ch1_lpddr4_trip1_cke_a : out STD_LOGIC;
    ch1_lpddr4_trip1_cke_b : out STD_LOGIC;
    ch1_lpddr4_trip1_cs_a : out STD_LOGIC;
    ch1_lpddr4_trip1_cs_b : out STD_LOGIC;
    ch1_lpddr4_trip1_dmi_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dmi_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dq_a : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1_lpddr4_trip1_dq_b : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1_lpddr4_trip1_dqs_c_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dqs_c_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dqs_t_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dqs_t_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_reset_n : out STD_LOGIC;
    ipb_ic_rst_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    lpddr4_clk1_clk_n : in STD_LOGIC;
    lpddr4_clk1_clk_p : in STD_LOGIC
  );
end vpk180_ipb_wrapper;

architecture STRUCTURE of vpk180_ipb_wrapper is
  component vpk180_ipb is
  port (
    ch0_lpddr4_trip1_dq_a : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch0_lpddr4_trip1_dq_b : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch0_lpddr4_trip1_dqs_t_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dqs_t_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dqs_c_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dqs_c_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_ca_a : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch0_lpddr4_trip1_ca_b : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch0_lpddr4_trip1_cs_a : out STD_LOGIC;
    ch0_lpddr4_trip1_cs_b : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_t_a : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_t_b : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_c_a : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_c_b : out STD_LOGIC;
    ch0_lpddr4_trip1_cke_a : out STD_LOGIC;
    ch0_lpddr4_trip1_cke_b : out STD_LOGIC;
    ch0_lpddr4_trip1_dmi_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dmi_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_reset_n : out STD_LOGIC;
    ch1_lpddr4_trip1_dq_a : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1_lpddr4_trip1_dq_b : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1_lpddr4_trip1_dqs_t_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dqs_t_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dqs_c_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dqs_c_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_ca_a : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch1_lpddr4_trip1_ca_b : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch1_lpddr4_trip1_cs_a : out STD_LOGIC;
    ch1_lpddr4_trip1_cs_b : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_t_a : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_t_b : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_c_a : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_c_b : out STD_LOGIC;
    ch1_lpddr4_trip1_cke_a : out STD_LOGIC;
    ch1_lpddr4_trip1_cke_b : out STD_LOGIC;
    ch1_lpddr4_trip1_dmi_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dmi_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_reset_n : out STD_LOGIC;
    lpddr4_clk1_clk_p : in STD_LOGIC;
    lpddr4_clk1_clk_n : in STD_LOGIC;
    axi_clk_o : out STD_LOGIC;
    ipb_ic_rst_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    BRAM_PORTA_addr : out STD_LOGIC_VECTOR ( 12 downto 0 );
    BRAM_PORTA_clk : out STD_LOGIC;
    BRAM_PORTA_din : out STD_LOGIC_VECTOR ( 63 downto 0 );
    BRAM_PORTA_dout : in STD_LOGIC_VECTOR ( 63 downto 0 );
    BRAM_PORTA_en : out STD_LOGIC;
    BRAM_PORTA_rst : out STD_LOGIC;
    BRAM_PORTA_we : out STD_LOGIC_VECTOR ( 7 downto 0 );
    BRAM_PORTB_addr : out STD_LOGIC_VECTOR ( 12 downto 0 );
    BRAM_PORTB_clk : out STD_LOGIC;
    BRAM_PORTB_din : out STD_LOGIC_VECTOR ( 63 downto 0 );
    BRAM_PORTB_dout : in STD_LOGIC_VECTOR ( 63 downto 0 );
    BRAM_PORTB_en : out STD_LOGIC;
    BRAM_PORTB_rst : out STD_LOGIC;
    BRAM_PORTB_we : out STD_LOGIC_VECTOR ( 7 downto 0 );
    bd_arst_o : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component vpk180_ipb;
begin
vpk180_ipb_i: component vpk180_ipb
     port map (
      BRAM_PORTA_addr(12 downto 0) => BRAM_PORTA_addr(12 downto 0),
      BRAM_PORTA_clk => BRAM_PORTA_clk,
      BRAM_PORTA_din(63 downto 0) => BRAM_PORTA_din(63 downto 0),
      BRAM_PORTA_dout(63 downto 0) => BRAM_PORTA_dout(63 downto 0),
      BRAM_PORTA_en => BRAM_PORTA_en,
      BRAM_PORTA_rst => BRAM_PORTA_rst,
      BRAM_PORTA_we(7 downto 0) => BRAM_PORTA_we(7 downto 0),
      BRAM_PORTB_addr(12 downto 0) => BRAM_PORTB_addr(12 downto 0),
      BRAM_PORTB_clk => BRAM_PORTB_clk,
      BRAM_PORTB_din(63 downto 0) => BRAM_PORTB_din(63 downto 0),
      BRAM_PORTB_dout(63 downto 0) => BRAM_PORTB_dout(63 downto 0),
      BRAM_PORTB_en => BRAM_PORTB_en,
      BRAM_PORTB_rst => BRAM_PORTB_rst,
      BRAM_PORTB_we(7 downto 0) => BRAM_PORTB_we(7 downto 0),
      axi_clk_o => axi_clk_o,
      bd_arst_o(0) => bd_arst_o(0),
      ch0_lpddr4_trip1_ca_a(5 downto 0) => ch0_lpddr4_trip1_ca_a(5 downto 0),
      ch0_lpddr4_trip1_ca_b(5 downto 0) => ch0_lpddr4_trip1_ca_b(5 downto 0),
      ch0_lpddr4_trip1_ck_c_a => ch0_lpddr4_trip1_ck_c_a,
      ch0_lpddr4_trip1_ck_c_b => ch0_lpddr4_trip1_ck_c_b,
      ch0_lpddr4_trip1_ck_t_a => ch0_lpddr4_trip1_ck_t_a,
      ch0_lpddr4_trip1_ck_t_b => ch0_lpddr4_trip1_ck_t_b,
      ch0_lpddr4_trip1_cke_a => ch0_lpddr4_trip1_cke_a,
      ch0_lpddr4_trip1_cke_b => ch0_lpddr4_trip1_cke_b,
      ch0_lpddr4_trip1_cs_a => ch0_lpddr4_trip1_cs_a,
      ch0_lpddr4_trip1_cs_b => ch0_lpddr4_trip1_cs_b,
      ch0_lpddr4_trip1_dmi_a(1 downto 0) => ch0_lpddr4_trip1_dmi_a(1 downto 0),
      ch0_lpddr4_trip1_dmi_b(1 downto 0) => ch0_lpddr4_trip1_dmi_b(1 downto 0),
      ch0_lpddr4_trip1_dq_a(15 downto 0) => ch0_lpddr4_trip1_dq_a(15 downto 0),
      ch0_lpddr4_trip1_dq_b(15 downto 0) => ch0_lpddr4_trip1_dq_b(15 downto 0),
      ch0_lpddr4_trip1_dqs_c_a(1 downto 0) => ch0_lpddr4_trip1_dqs_c_a(1 downto 0),
      ch0_lpddr4_trip1_dqs_c_b(1 downto 0) => ch0_lpddr4_trip1_dqs_c_b(1 downto 0),
      ch0_lpddr4_trip1_dqs_t_a(1 downto 0) => ch0_lpddr4_trip1_dqs_t_a(1 downto 0),
      ch0_lpddr4_trip1_dqs_t_b(1 downto 0) => ch0_lpddr4_trip1_dqs_t_b(1 downto 0),
      ch0_lpddr4_trip1_reset_n => ch0_lpddr4_trip1_reset_n,
      ch1_lpddr4_trip1_ca_a(5 downto 0) => ch1_lpddr4_trip1_ca_a(5 downto 0),
      ch1_lpddr4_trip1_ca_b(5 downto 0) => ch1_lpddr4_trip1_ca_b(5 downto 0),
      ch1_lpddr4_trip1_ck_c_a => ch1_lpddr4_trip1_ck_c_a,
      ch1_lpddr4_trip1_ck_c_b => ch1_lpddr4_trip1_ck_c_b,
      ch1_lpddr4_trip1_ck_t_a => ch1_lpddr4_trip1_ck_t_a,
      ch1_lpddr4_trip1_ck_t_b => ch1_lpddr4_trip1_ck_t_b,
      ch1_lpddr4_trip1_cke_a => ch1_lpddr4_trip1_cke_a,
      ch1_lpddr4_trip1_cke_b => ch1_lpddr4_trip1_cke_b,
      ch1_lpddr4_trip1_cs_a => ch1_lpddr4_trip1_cs_a,
      ch1_lpddr4_trip1_cs_b => ch1_lpddr4_trip1_cs_b,
      ch1_lpddr4_trip1_dmi_a(1 downto 0) => ch1_lpddr4_trip1_dmi_a(1 downto 0),
      ch1_lpddr4_trip1_dmi_b(1 downto 0) => ch1_lpddr4_trip1_dmi_b(1 downto 0),
      ch1_lpddr4_trip1_dq_a(15 downto 0) => ch1_lpddr4_trip1_dq_a(15 downto 0),
      ch1_lpddr4_trip1_dq_b(15 downto 0) => ch1_lpddr4_trip1_dq_b(15 downto 0),
      ch1_lpddr4_trip1_dqs_c_a(1 downto 0) => ch1_lpddr4_trip1_dqs_c_a(1 downto 0),
      ch1_lpddr4_trip1_dqs_c_b(1 downto 0) => ch1_lpddr4_trip1_dqs_c_b(1 downto 0),
      ch1_lpddr4_trip1_dqs_t_a(1 downto 0) => ch1_lpddr4_trip1_dqs_t_a(1 downto 0),
      ch1_lpddr4_trip1_dqs_t_b(1 downto 0) => ch1_lpddr4_trip1_dqs_t_b(1 downto 0),
      ch1_lpddr4_trip1_reset_n => ch1_lpddr4_trip1_reset_n,
      ipb_ic_rst_o(0) => ipb_ic_rst_o(0),
      lpddr4_clk1_clk_n => lpddr4_clk1_clk_n,
      lpddr4_clk1_clk_p => lpddr4_clk1_clk_p
    );
end STRUCTURE;

