library ieee;
use ieee.std_logic_1164.all;

--library xil_defaultlib;
--use xil_defaultlib.all;

library ipbus;
use ipbus.ipbus.all;
use ipbus.ipbus_trans_decl.all;
use ipbus.ipbus_axi_decl.all;

entity top is 
  port(
    ext_rst_in          : in  std_logic
    --ext_clk             : in  std_logic;
    --axi_in              : in axi4mm_ms(araddr(63 downto 0), awaddr(63 downto 0), wdata(63 downto 0));
	--axi_out             : out axi4mm_sm(rdata(63 downto 0))
  );

end top;

architecture rtl of top is
  
 
  signal bd_clk_o_s  : std_logic;
  signal bd_rst_o_s  : std_logic;
  
  signal ipb_out  : ipb_wbus;
  signal ipb_in   : ipb_rbus;
  
  signal axi_ms : axi4mm_ms(araddr(63 downto 0), awaddr(63 downto 0), wdata(63 downto 0));
  signal axi_sm : axi4mm_sm(rdata(63 downto 0));
  signal trans_in     : ipbus_trans_in;
  signal trans_out    : ipbus_trans_out;
  
begin


  axi_ms.aclk <= bd_clk_o_s;
    
  zybo_bd: entity work.zybo_ipb_wrapper
  port map (
  
     ipb_axi_awready     => axi_sm.awready,
     ipb_axi_wready      => axi_sm.wready,
--   ipb_axi_bid         => axi_sm.bid,
     ipb_axi_bresp       => axi_sm.bresp,
     ipb_axi_bvalid      => axi_sm.bvalid,
     ipb_axi_arready     => axi_sm.arready,
--   ipb_axi_rid         => axi_sm.rid,
     ipb_axi_rdata       => axi_sm.rdata,
     ipb_axi_rresp       => axi_sm.rresp,
     ipb_axi_rlast       => axi_sm.rlast,
     ipb_axi_rvalid      => axi_sm.rvalid,
--   ipb_axi_awid        => axi_ms.awid,
     ipb_axi_awaddr(15 downto 0) => axi_ms.awaddr(15 downto 0), ipb_axi_awaddr(31 downto 16) => open,
     ipb_axi_awlen       => axi_ms.awlen,
     ipb_axi_awsize      => axi_ms.awsize,
     ipb_axi_awburst     => axi_ms.awburst,
     ipb_axi_awprot      => axi_ms.awprot,
     ipb_axi_awvalid     => axi_ms.awvalid,
     ipb_axi_awlock(0)   => axi_ms.awlock,
     ipb_axi_awcache     => axi_ms.awcache,
     ipb_axi_wdata       => axi_ms.wdata,
     ipb_axi_wstrb       => axi_ms.wstrb,
     ipb_axi_wlast       => axi_ms.wlast,
     ipb_axi_wvalid      => axi_ms.wvalid,
     ipb_axi_bready      => axi_ms.bready,
--   ipb_axi_arid        => axi_ms.arid,
     ipb_axi_araddr(15 downto 0) => axi_ms.araddr(15 downto 0), ipb_axi_araddr(31 downto 16) => open,
     ipb_axi_arlen       => axi_ms.arlen,
     ipb_axi_arsize      => axi_ms.arsize,
     ipb_axi_arburst     => axi_ms.arburst,
     ipb_axi_arprot      => axi_ms.arprot,
     ipb_axi_arvalid     => axi_ms.arvalid,
     ipb_axi_arlock(0)   => axi_ms.arlock,
     ipb_axi_arcache     => axi_ms.arcache,
     ipb_axi_rready      => axi_ms.rready,
     
     axi_clk_o            =>  bd_clk_o_s,
     axi_rst_o(0)        =>  axi_ms.aresetn,
     
     ipb_ic_rst_o(0)     => bd_rst_o_s,
     --ipb_periph_rsl_o(0) => open,
     ext_rst_i(0)        => ext_rst_in
  
  );

  ipb_axi_inst: entity ipbus.ipbus_transport_axi_if
    port map (
      ipb_clk         => bd_clk_o_s,
      rst_ipb         => bd_rst_o_s,
      axi_in          => axi_ms,
      axi_out         => axi_sm,
      pkt_done        => open,
      ipb_trans_rx    => trans_in, 
      ipb_trans_tx    => trans_out
    );


  ipb_trans_inst: entity ipbus.transactor
    port map (
      clk             => bd_clk_o_s,
      rst             => bd_rst_o_s,
      ipb_out         => ipb_out,
      ipb_in          => ipb_in,
      ipb_req         => open,
      ipb_grant       => '1',
      trans_in        => trans_in,
      trans_out       => trans_out,
      cfg_vector_in   => (others => '0'),
      cfg_vector_out  => open
    );
    
  local_slaves: entity ipbus.ipbus_example
    port map(
      ipb_clk    => bd_clk_o_s,
      ipb_rst    => bd_rst_o_s,
      ipb_in     => ipb_out,
      ipb_out    => ipb_in
      -- userled    => '1',
      ---status(19 downto 18) => "00",
      ---status(31 downto 20) => X"abc",
      ---status(0 downto 17) => "010101010101010101"
      );

end rtl;