--Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2020.2 (lin64) Build 3064766 Wed Nov 18 09:12:47 MST 2020
--Date        : Tue Feb 13 11:37:56 2024
--Host        : epldt002.ph.bham.ac.uk running 64-bit Fedora release 36 (Thirty Six)
--Command     : generate_target zybo_ipb_wrapper.bd
--Design      : zybo_ipb_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity zybo_ipb_wrapper is
  port (
    DDR_0_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_0_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_0_cas_n : inout STD_LOGIC;
    DDR_0_ck_n : inout STD_LOGIC;
    DDR_0_ck_p : inout STD_LOGIC;
    DDR_0_cke : inout STD_LOGIC;
    DDR_0_cs_n : inout STD_LOGIC;
    DDR_0_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_0_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_0_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_0_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_0_odt : inout STD_LOGIC;
    DDR_0_ras_n : inout STD_LOGIC;
    DDR_0_reset_n : inout STD_LOGIC;
    DDR_0_we_n : inout STD_LOGIC;
    FIXED_IO_0_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_0_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_0_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_0_ps_clk : inout STD_LOGIC;
    FIXED_IO_0_ps_porb : inout STD_LOGIC;
    FIXED_IO_0_ps_srstb : inout STD_LOGIC;
    axi_clk_o : out STD_LOGIC;
    axi_rst_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    ext_rst_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    ipb_axi_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ipb_axi_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ipb_axi_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ipb_axi_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ipb_axi_arlock : out STD_LOGIC_VECTOR ( 0 to 0 );
    ipb_axi_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ipb_axi_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ipb_axi_arready : in STD_LOGIC;
    ipb_axi_arregion : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ipb_axi_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ipb_axi_arvalid : out STD_LOGIC;
    ipb_axi_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ipb_axi_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ipb_axi_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ipb_axi_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ipb_axi_awlock : out STD_LOGIC_VECTOR ( 0 to 0 );
    ipb_axi_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ipb_axi_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ipb_axi_awready : in STD_LOGIC;
    ipb_axi_awregion : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ipb_axi_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ipb_axi_awvalid : out STD_LOGIC;
    ipb_axi_bready : out STD_LOGIC;
    ipb_axi_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ipb_axi_bvalid : in STD_LOGIC;
    ipb_axi_rdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    ipb_axi_rlast : in STD_LOGIC;
    ipb_axi_rready : out STD_LOGIC;
    ipb_axi_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ipb_axi_rvalid : in STD_LOGIC;
    ipb_axi_wdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    ipb_axi_wlast : out STD_LOGIC;
    ipb_axi_wready : in STD_LOGIC;
    ipb_axi_wstrb : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ipb_axi_wvalid : out STD_LOGIC;
    ipb_ic_rst_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    ipb_periph_rsl_o : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
end zybo_ipb_wrapper;

architecture STRUCTURE of zybo_ipb_wrapper is
  component zybo_ipb is
  port (
    axi_clk_o : out STD_LOGIC;
    ext_rst_i : in STD_LOGIC_VECTOR ( 0 to 0 );
    ipb_ic_rst_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    ipb_periph_rsl_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    axi_rst_o : out STD_LOGIC_VECTOR ( 0 to 0 );
    ipb_axi_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ipb_axi_awlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ipb_axi_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ipb_axi_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ipb_axi_awlock : out STD_LOGIC_VECTOR ( 0 to 0 );
    ipb_axi_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ipb_axi_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ipb_axi_awregion : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ipb_axi_awqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ipb_axi_awvalid : out STD_LOGIC;
    ipb_axi_awready : in STD_LOGIC;
    ipb_axi_wdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    ipb_axi_wstrb : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ipb_axi_wlast : out STD_LOGIC;
    ipb_axi_wvalid : out STD_LOGIC;
    ipb_axi_wready : in STD_LOGIC;
    ipb_axi_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ipb_axi_bvalid : in STD_LOGIC;
    ipb_axi_bready : out STD_LOGIC;
    ipb_axi_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ipb_axi_arlen : out STD_LOGIC_VECTOR ( 7 downto 0 );
    ipb_axi_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ipb_axi_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ipb_axi_arlock : out STD_LOGIC_VECTOR ( 0 to 0 );
    ipb_axi_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ipb_axi_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    ipb_axi_arregion : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ipb_axi_arqos : out STD_LOGIC_VECTOR ( 3 downto 0 );
    ipb_axi_arvalid : out STD_LOGIC;
    ipb_axi_arready : in STD_LOGIC;
    ipb_axi_rdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    ipb_axi_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    ipb_axi_rlast : in STD_LOGIC;
    ipb_axi_rvalid : in STD_LOGIC;
    ipb_axi_rready : out STD_LOGIC;
    DDR_0_cas_n : inout STD_LOGIC;
    DDR_0_cke : inout STD_LOGIC;
    DDR_0_ck_n : inout STD_LOGIC;
    DDR_0_ck_p : inout STD_LOGIC;
    DDR_0_cs_n : inout STD_LOGIC;
    DDR_0_reset_n : inout STD_LOGIC;
    DDR_0_odt : inout STD_LOGIC;
    DDR_0_ras_n : inout STD_LOGIC;
    DDR_0_we_n : inout STD_LOGIC;
    DDR_0_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_0_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_0_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_0_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_0_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_0_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    FIXED_IO_0_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_0_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_0_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_0_ps_srstb : inout STD_LOGIC;
    FIXED_IO_0_ps_clk : inout STD_LOGIC;
    FIXED_IO_0_ps_porb : inout STD_LOGIC
  );
  end component zybo_ipb;
begin
zybo_ipb_i: component zybo_ipb
     port map (
      DDR_0_addr(14 downto 0) => DDR_0_addr(14 downto 0),
      DDR_0_ba(2 downto 0) => DDR_0_ba(2 downto 0),
      DDR_0_cas_n => DDR_0_cas_n,
      DDR_0_ck_n => DDR_0_ck_n,
      DDR_0_ck_p => DDR_0_ck_p,
      DDR_0_cke => DDR_0_cke,
      DDR_0_cs_n => DDR_0_cs_n,
      DDR_0_dm(3 downto 0) => DDR_0_dm(3 downto 0),
      DDR_0_dq(31 downto 0) => DDR_0_dq(31 downto 0),
      DDR_0_dqs_n(3 downto 0) => DDR_0_dqs_n(3 downto 0),
      DDR_0_dqs_p(3 downto 0) => DDR_0_dqs_p(3 downto 0),
      DDR_0_odt => DDR_0_odt,
      DDR_0_ras_n => DDR_0_ras_n,
      DDR_0_reset_n => DDR_0_reset_n,
      DDR_0_we_n => DDR_0_we_n,
      FIXED_IO_0_ddr_vrn => FIXED_IO_0_ddr_vrn,
      FIXED_IO_0_ddr_vrp => FIXED_IO_0_ddr_vrp,
      FIXED_IO_0_mio(53 downto 0) => FIXED_IO_0_mio(53 downto 0),
      FIXED_IO_0_ps_clk => FIXED_IO_0_ps_clk,
      FIXED_IO_0_ps_porb => FIXED_IO_0_ps_porb,
      FIXED_IO_0_ps_srstb => FIXED_IO_0_ps_srstb,
      axi_clk_o => axi_clk_o,
      axi_rst_o(0) => axi_rst_o(0),
      ext_rst_i(0) => ext_rst_i(0),
      ipb_axi_araddr(31 downto 0) => ipb_axi_araddr(31 downto 0),
      ipb_axi_arburst(1 downto 0) => ipb_axi_arburst(1 downto 0),
      ipb_axi_arcache(3 downto 0) => ipb_axi_arcache(3 downto 0),
      ipb_axi_arlen(7 downto 0) => ipb_axi_arlen(7 downto 0),
      ipb_axi_arlock(0) => ipb_axi_arlock(0),
      ipb_axi_arprot(2 downto 0) => ipb_axi_arprot(2 downto 0),
      ipb_axi_arqos(3 downto 0) => ipb_axi_arqos(3 downto 0),
      ipb_axi_arready => ipb_axi_arready,
      ipb_axi_arregion(3 downto 0) => ipb_axi_arregion(3 downto 0),
      ipb_axi_arsize(2 downto 0) => ipb_axi_arsize(2 downto 0),
      ipb_axi_arvalid => ipb_axi_arvalid,
      ipb_axi_awaddr(31 downto 0) => ipb_axi_awaddr(31 downto 0),
      ipb_axi_awburst(1 downto 0) => ipb_axi_awburst(1 downto 0),
      ipb_axi_awcache(3 downto 0) => ipb_axi_awcache(3 downto 0),
      ipb_axi_awlen(7 downto 0) => ipb_axi_awlen(7 downto 0),
      ipb_axi_awlock(0) => ipb_axi_awlock(0),
      ipb_axi_awprot(2 downto 0) => ipb_axi_awprot(2 downto 0),
      ipb_axi_awqos(3 downto 0) => ipb_axi_awqos(3 downto 0),
      ipb_axi_awready => ipb_axi_awready,
      ipb_axi_awregion(3 downto 0) => ipb_axi_awregion(3 downto 0),
      ipb_axi_awsize(2 downto 0) => ipb_axi_awsize(2 downto 0),
      ipb_axi_awvalid => ipb_axi_awvalid,
      ipb_axi_bready => ipb_axi_bready,
      ipb_axi_bresp(1 downto 0) => ipb_axi_bresp(1 downto 0),
      ipb_axi_bvalid => ipb_axi_bvalid,
      ipb_axi_rdata(63 downto 0) => ipb_axi_rdata(63 downto 0),
      ipb_axi_rlast => ipb_axi_rlast,
      ipb_axi_rready => ipb_axi_rready,
      ipb_axi_rresp(1 downto 0) => ipb_axi_rresp(1 downto 0),
      ipb_axi_rvalid => ipb_axi_rvalid,
      ipb_axi_wdata(63 downto 0) => ipb_axi_wdata(63 downto 0),
      ipb_axi_wlast => ipb_axi_wlast,
      ipb_axi_wready => ipb_axi_wready,
      ipb_axi_wstrb(7 downto 0) => ipb_axi_wstrb(7 downto 0),
      ipb_axi_wvalid => ipb_axi_wvalid,
      ipb_ic_rst_o(0) => ipb_ic_rst_o(0),
      ipb_periph_rsl_o(0) => ipb_periph_rsl_o(0)
    );
end STRUCTURE;
