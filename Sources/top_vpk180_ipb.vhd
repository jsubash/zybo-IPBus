library ieee;
use ieee.std_logic_1164.all;

--library xil_defaultlib;
-- use xil_defaultlib.all;

library ipbus;
use ipbus.ipbus.all;
use ipbus.ipbus_trans_decl.all;
use ipbus.ipbus_axi_decl.all;

entity top is 
  --generic(
  --BUFWIDTH: natural := 1;
  --ADDRWIDTH: natural := 11
  -- );
  port (
    ch0_lpddr4_trip1_dq_a : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch0_lpddr4_trip1_dq_b : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch0_lpddr4_trip1_dqs_t_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dqs_t_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dqs_c_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dqs_c_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_ca_a : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch0_lpddr4_trip1_ca_b : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch0_lpddr4_trip1_cs_a : out STD_LOGIC;
    ch0_lpddr4_trip1_cs_b : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_t_a : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_t_b : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_c_a : out STD_LOGIC;
    ch0_lpddr4_trip1_ck_c_b : out STD_LOGIC;
    ch0_lpddr4_trip1_cke_a : out STD_LOGIC;
    ch0_lpddr4_trip1_cke_b : out STD_LOGIC;
    ch0_lpddr4_trip1_dmi_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_dmi_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch0_lpddr4_trip1_reset_n : out STD_LOGIC;
    ch1_lpddr4_trip1_dq_a : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1_lpddr4_trip1_dq_b : inout STD_LOGIC_VECTOR ( 15 downto 0 );
    ch1_lpddr4_trip1_dqs_t_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dqs_t_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dqs_c_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dqs_c_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_ca_a : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch1_lpddr4_trip1_ca_b : out STD_LOGIC_VECTOR ( 5 downto 0 );
    ch1_lpddr4_trip1_cs_a : out STD_LOGIC;
    ch1_lpddr4_trip1_cs_b : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_t_a : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_t_b : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_c_a : out STD_LOGIC;
    ch1_lpddr4_trip1_ck_c_b : out STD_LOGIC;
    ch1_lpddr4_trip1_cke_a : out STD_LOGIC;
    ch1_lpddr4_trip1_cke_b : out STD_LOGIC;
    ch1_lpddr4_trip1_dmi_a : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_dmi_b : inout STD_LOGIC_VECTOR ( 1 downto 0 );
    ch1_lpddr4_trip1_reset_n : out STD_LOGIC;
    lpddr4_clk1_clk_p : in STD_LOGIC;
    lpddr4_clk1_clk_n : in STD_LOGIC
    );
end top;

architecture rtl of top is
  
  
  signal bd_clk_o_s  : std_logic;
  signal bd_rst_o_s  : std_logic;
  signal bd_arst_o_s : STD_LOGIC_VECTOR ( 0 to 0 );
  signal ipb_out  : ipb_wbus;
  signal ipb_in   : ipb_rbus;
  
  signal bram_addr_a, bram_addr_b : std_logic_vector(12 downto 0);
  signal ram_wr_en, ram_we, ram_rd_en : std_logic;
  signal ram_wr_we : std_logic_vector(7 downto 0);
  signal ram_wr_data, ram_rd_data : std_logic_vector(63 downto 0);
  signal ipb_trans_in     : ipbus_trans_in;
  signal ipb_trans_out    : ipbus_trans_out;
  
begin

  ram_we <= bd_arst_o_s(0) and ram_wr_en and ram_wr_we(0);
  
  vpk180_bd: entity work.vpk180_ipb_wrapper
    port map (
      ch0_lpddr4_trip1_dq_a  => ch0_lpddr4_trip1_dq_a,
      ch0_lpddr4_trip1_dq_b => ch0_lpddr4_trip1_dq_b,
      ch0_lpddr4_trip1_dqs_t_a => ch0_lpddr4_trip1_dqs_t_a,
      ch0_lpddr4_trip1_dqs_t_b => ch0_lpddr4_trip1_dqs_t_b,
      ch0_lpddr4_trip1_dqs_c_a => ch0_lpddr4_trip1_dqs_c_a,
      ch0_lpddr4_trip1_dqs_c_b => ch0_lpddr4_trip1_dqs_c_b,
      ch0_lpddr4_trip1_ca_a => ch0_lpddr4_trip1_ca_a,
      ch0_lpddr4_trip1_ca_b => ch0_lpddr4_trip1_ca_b,
      ch0_lpddr4_trip1_cs_a=> ch0_lpddr4_trip1_cs_a,
      ch0_lpddr4_trip1_cs_b => ch0_lpddr4_trip1_cs_b,
      ch0_lpddr4_trip1_ck_t_a => ch0_lpddr4_trip1_ck_t_a,
      ch0_lpddr4_trip1_ck_t_b => ch0_lpddr4_trip1_ck_t_b,
      ch0_lpddr4_trip1_ck_c_a => ch0_lpddr4_trip1_ck_c_a,
      ch0_lpddr4_trip1_ck_c_b => ch0_lpddr4_trip1_ck_c_b,
      ch0_lpddr4_trip1_cke_a => ch0_lpddr4_trip1_cke_a,
      ch0_lpddr4_trip1_cke_b => ch0_lpddr4_trip1_cke_b,
      ch0_lpddr4_trip1_dmi_a => ch0_lpddr4_trip1_dmi_a,
      ch0_lpddr4_trip1_dmi_b => ch0_lpddr4_trip1_dmi_b,
      ch0_lpddr4_trip1_reset_n => ch0_lpddr4_trip1_reset_n,
      ch1_lpddr4_trip1_dq_a => ch1_lpddr4_trip1_dq_a,
      ch1_lpddr4_trip1_dq_b => ch1_lpddr4_trip1_dq_b,
      ch1_lpddr4_trip1_dqs_t_a => ch1_lpddr4_trip1_dqs_t_a,
      ch1_lpddr4_trip1_dqs_t_b => ch1_lpddr4_trip1_dqs_t_b,
      ch1_lpddr4_trip1_dqs_c_a => ch1_lpddr4_trip1_dqs_c_a,
      ch1_lpddr4_trip1_dqs_c_b => ch1_lpddr4_trip1_dqs_c_b,
      ch1_lpddr4_trip1_ca_a => ch1_lpddr4_trip1_ca_a,
      ch1_lpddr4_trip1_ca_b => ch1_lpddr4_trip1_ca_b,
      ch1_lpddr4_trip1_cs_a => ch1_lpddr4_trip1_cs_a,
      ch1_lpddr4_trip1_cs_b => ch1_lpddr4_trip1_cs_b,
      ch1_lpddr4_trip1_ck_t_a => ch1_lpddr4_trip1_ck_t_a,
      ch1_lpddr4_trip1_ck_t_b => ch1_lpddr4_trip1_ck_t_b,
      ch1_lpddr4_trip1_ck_c_a => ch1_lpddr4_trip1_ck_c_a,
      ch1_lpddr4_trip1_ck_c_b => ch1_lpddr4_trip1_ck_c_b,
      ch1_lpddr4_trip1_cke_a => ch1_lpddr4_trip1_cke_a,
      ch1_lpddr4_trip1_cke_b => ch1_lpddr4_trip1_cke_b,
      ch1_lpddr4_trip1_dmi_a => ch1_lpddr4_trip1_dmi_a,
      ch1_lpddr4_trip1_dmi_b => ch1_lpddr4_trip1_dmi_b,
      ch1_lpddr4_trip1_reset_n => ch1_lpddr4_trip1_reset_n,
      
      lpddr4_clk1_clk_n => lpddr4_clk1_clk_n,
      lpddr4_clk1_clk_p => lpddr4_clk1_clk_p,
      
      BRAM_PORTA_rst  =>  open,
      BRAM_PORTA_clk  =>  open,
      BRAM_PORTA_en   =>  ram_wr_en,
      BRAM_PORTA_we   =>  ram_wr_we,
      BRAM_PORTA_addr =>  bram_addr_a,
      BRAM_PORTA_din  =>  ram_wr_data,
      BRAM_PORTA_dout => X"0000000000000000",
      
      BRAM_PORTB_rst  =>  open,
      BRAM_PORTB_clk  =>  open,
      BRAM_PORTB_en   =>  ram_rd_en,
      BRAM_PORTB_we   =>  open,
      BRAM_PORTB_addr =>  bram_addr_b,
      BRAM_PORTB_din  =>  open,
      BRAM_PORTB_dout => ram_rd_data,
      
      axi_clk_o       =>  bd_clk_o_s,
      bd_arst_o       => bd_arst_o_s,
      ipb_ic_rst_o(0) => bd_rst_o_s
      );
  
  ram_to_trans : entity ipbus.ipbus_transport_ram_if                                                                                                                                                                                     
    generic map (                                                                                                                                                                                                                 
      BUFWIDTH => 2,                                                                                                                                                                                                 
      ADDRWIDTH => 11                                                                                                                                                                                                
      )                                                                                                                                                                                                                             
    port map (                                                                                                                                                                                                                    
      ram_clk => bd_clk_o_s,                                                                                                                                                                                                   
      ipb_clk => bd_clk_o_s,                                                                                                                                                                                                   
      rst_ipb => bd_rst_o_s,                                                                                                                                                                                                   
      wr_addr => bram_addr_a (12 downto 1),                                                                                                                                                           
      wr_data => ram_wr_data,                                                                                                                                                                                               
      wr_en => ram_we,                                                                                                                                                                                                      
      
      rd_addr => bram_addr_b (12 downto 0),                                                                                                                                                           
      rd_data => ram_rd_data,                                                                                                                                                                                               
      
      pkt_done => open,                                                                                                                                                                                                 
      
      trans_in => ipb_trans_in,                                                                                                                                                                                             
      trans_out => ipb_trans_out                                                                                                                                                                                             
      );  


  ipb_trans_inst: entity ipbus.transactor
    port map (
      clk             => bd_clk_o_s,
      rst             => bd_rst_o_s,
      ipb_out         => ipb_out,
      ipb_in          => ipb_in,
      ipb_req         => open,
      ipb_grant       => '1',
      trans_in        => ipb_trans_in,
      trans_out       => ipb_trans_out,
      cfg_vector_in   => (others => '0'),
      cfg_vector_out  => open
      );
  
  local_slaves: entity ipbus.ipbus_example
    port map(
      ipb_clk    => bd_clk_o_s,
      ipb_rst    => bd_rst_o_s,
      ipb_in     => ipb_out,
      ipb_out    => ipb_in
      );

end rtl;
